angular.module('xeenTool').component('accessorytab', {
	controller: AccessoryTabController,
    bindings: {
        accessories: '<', 
        qualities: '<', 
        },
	templateUrl: 'app/components/accessorytab/accessorytab.template.html',
});

function AccessoryTabController() {
	this.sortCol = "t_ac";
	this.sortRev = true;
    this.saccessory = null;
    this.saquality = null;

    this.aentries = []; 

    this.aid = 0; 

    this.addClicked = function() {
        entry = {
            "aname": this.saccessory["name"],
            "type": this.saccessory["type"],
            "qname": this.saquality["name"],
            "enhancement": this.saquality["enhancement"],
            "emagnitude": this.saquality["emagnitude"],
            "mark": "normal",
            "id": this.aid,
        };
        this.aentries.push(entry);
        this.aid++;
    };

    this.clearClicked = function() {
        for (var i = 0; i < this.aentries.length; ++i) {
            if (this.aentries[i].mark != "keep") {
                this.aentries.splice(i, 1);
                i--;
            }
        }
    };

    this.removeClicked = function(id) {
        for (var i = 0; i < this.aentries.length; ++i) {
            if (this.aentries[i].id == id) {
                this.aentries.splice(i, 1);
                i--;
            }
        }
    };
    
    this.markClicked = function(wentry) {
        if (wentry.mark == "normal") {
            wentry.mark = "keep";
        }
        else if (wentry.mark == "keep") {
            wentry.mark = "remove";
        }
        else if (wentry.mark == "remove") {
            wentry.mark = "normal";
        }
    };
    
	this.setColSort = function (colName) {
        if (this.sortCol == colName)
		    this.sortRev = !this.sortRev;
		this.sortCol = colName;
	}
}

