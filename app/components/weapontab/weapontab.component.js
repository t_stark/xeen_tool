angular.module('xeenTool').component('weapontab', {
	controller: WeaponTabController,
    bindings: {
        weapons: '<', 
        qualities: '<', 
        },
	templateUrl: 'app/components/weapontab/weapontab.template.html',
});

function WeaponTabController() {
	this.sortCol = "t_avg_dmg";
	this.sortRev = true;
    this.sweapon = null;
    this.swquality = null;

    this.wentries = []; 

    this.wid = 0;

    this.addClicked = function() {
        entry = {
            "wname": this.sweapon["name"],
            "dice": this.sweapon["dice"],
            "classes": this.sweapon["classes"],
            "hands": this.sweapon["hands"],
            "avg_damage": this.sweapon["avg_damage"],
            "qname": this.swquality["name"],
            "damagebonus": this.swquality["damagebonus"],
            "tohitbonus": this.swquality["tohitbonus"],
            "dbonustype": this.swquality["dbonustype"],
            "mark": "normal",
            "t_avg_dmg": this.sweapon["avg_damage"] + this.swquality["damagebonus"],
            "id": this.wid,
        };
        this.wentries.push(entry);
        this.wid++;
    };

    this.clearClicked = function() {
        for (var i = 0; i < this.wentries.length; ++i) {
            if (this.wentries[i].mark != "keep") {
                this.wentries.splice(i, 1);
                i--;
            }
        }
    };

    this.removeClicked = function(id) {
        for (var i = 0; i < this.wentries.length; ++i) {
            if (this.wentries[i].id == id) {
                this.wentries.splice(i, 1);
                i--;
            }
        }
    };
    
    this.markClicked = function(wentry) {
        if (wentry.mark == "normal") {
            wentry.mark = "keep";
        }
        else if (wentry.mark == "keep") {
            wentry.mark = "remove";
        }
        else if (wentry.mark == "remove") {
            wentry.mark = "normal";
        }
    };
    
	this.setColSort = function (colName) {
        if (this.sortCol == colName)
		    this.sortRev = !this.sortRev;
		this.sortCol = colName;
	}
}

angular.module('xeenTool').filter('excludedtypenone', function() {
    return function(qualities) {
        var wqualities = [];
        for (var i = 0; i < qualities.length; ++i) {
            if (qualities[i].dbonustype != "none") {
                wqualities.push(qualities[i]);
            } 
        }
        return wqualities;
    };
});

