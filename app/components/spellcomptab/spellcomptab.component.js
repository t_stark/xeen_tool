angular.module('xeenTool').component('spellcomptab', {
	controller: SpellCompTabController,
    bindings: {
        spells: '<', 
        },
	templateUrl: 'app/components/spellcomptab/spellcomptab.template.html',
});

function SpellCompTabController() {
    this.slevel;
    this.stype;
    this.sspell;
    this.slist;
	this.sortCol = "name";
	this.sortRev = true;
    this.sid = 0;

    this.entries = []; 

    this.levels = [
        { "name": "1", "value": 1, },
        { "name": "2", "value": 2, },
        { "name": "3", "value": 3, },
        { "name": "4", "value": 4, },
        { "name": "5", "value": 5, },
        { "name": "6", "value": 6, },
        { "name": "7", "value": 7, },
        { "name": "8", "value": 8, },
        { "name": "9", "value": 9, },
        { "name": "10", "value": 10, },
        { "name": "11", "value": 11, },
        { "name": "12", "value": 12, },
        { "name": "13", "value": 13, },
        { "name": "14", "value": 14, },
        { "name": "15", "value": 15, },
        { "name": "16", "value": 16, },
        { "name": "17", "value": 17, },
        { "name": "18", "value": 18, },
        { "name": "19", "value": 19, },
        { "name": "20", "value": 20, },
        { "name": "21", "value": 21, },
        { "name": "22", "value": 22, },
        { "name": "23", "value": 23, },
        { "name": "24", "value": 24, },
        { "name": "25", "value": 25, },
        { "name": "26", "value": 26, },
        { "name": "27", "value": 27, },
        { "name": "28", "value": 28, },
        { "name": "29", "value": 29, },
        { "name": "30", "value": 30, },
        { "name": "31", "value": 31, },
        { "name": "32", "value": 32, },
        { "name": "33", "value": 33, },
        { "name": "34", "value": 34, },
        { "name": "35", "value": 35, },
        { "name": "36", "value": 36, },
        { "name": "37", "value": 37, },
        { "name": "38", "value": 38, },
        { "name": "39", "value": 39, },
        { "name": "40", "value": 40, },
        { "name": "41", "value": 41, },
        { "name": "42", "value": 42, },
        { "name": "43", "value": 43, },
        { "name": "44", "value": 44, },
        { "name": "45", "value": 45, },
        { "name": "46", "value": 46, },
        { "name": "47", "value": 47, },
        { "name": "48", "value": 48, },
        { "name": "49", "value": 49, },
    ];

    this.types = [
        {
            "name": "Damage",
        },
        {
            "name": "Healing",
        },
    ];

    this.lists = [
        {
            "name": "Arcane",
            "value": "a",
        },
        {
            "name": "Clerical",
            "value": "c",
        },
        {
            "name": "Druid",
            "value": "d",
        },
    ];

    this.addClicked = function() {
        entry = {
            "name": this.sspell["name"],
            "desc": this.sspell["desc"],
            "base_sp_cost": this.sspell["base_sp_cost"],
            "sp_per_lvl": this.sspell["sp_per_lvl"],
            "base_g_cost": this.sspell["base_g_cost"],
            "g_per_lvl": this.sspell["g_per_lvl"],
            "spell_lists": this.sspell["spell_lists"],
            "dmg_type": this.sspell["dmg_type"],
            "base_dmg_amt": this.sspell["base_dmg_amt"],
            "dmg_per_lvl": this.sspell["dmg_per_lvl"],
            "dmg_targets": this.sspell["dmg_targets"],
            "base_healing_amt": this.sspell["base_healing_amt"],
            "healing_per_lvl": this.sspell["healing_per_lvl"],
            "healing_targets": this.sspell["healing_targets"],
            "level": this.slevel["value"],
            "t_g_cost": this.sspell["base_g_cost"] + (this.sspell["g_per_lvl"] * this.slevel["value"]),
            "t_sp_cost": this.sspell["base_sp_cost"] + (this.sspell["sp_per_lvl"] * this.slevel["value"]),
            "t_dmg": this.sspell["base_dmg_amt"] + (this.sspell["dmg_per_lvl"] * this.slevel["value"]),
            "t_healing": this.sspell["base_healing_amt"] + (this.sspell["healing_per_lvl"] * this.slevel["value"]),
            "mark": "normal",
            "id": this.sid,
        };
        this.entries.push(entry);
        this.sid++;
    };

    this.clearClicked = function() {
        for (var i = 0; i < this.entries.length; ++i) {
            if (this.entries[i].mark != "keep") {
                this.entries.splice(i, 1);
                i--;
            }
        }
    };

    this.removeClicked = function(id) {
        for (var i = 0; i < this.entries.length; ++i) {
            if (this.entries[i].id == id) {
                this.entries.splice(i, 1);
                i--;
            }
        }
    };
    
    this.lvlCalcClicked = function() {
        for (var i = 0; i < this.entries.length; ++i) {
            this.entries[i].t_g_cost = this.entries[i].base_g_cost + (this.entries[i].g_per_lvl * this.slevel["value"]);
            this.entries[i].t_sp_cost = this.entries[i].base_sp_cost + (this.entries[i].sp_per_lvl * this.slevel["value"]);
            this.entries[i].t_dmg = this.entries[i].base_dmg_amt + (this.entries[i].dmg_per_lvl * this.slevel["value"]);
            this.entries[i].t_healing = this.entries[i].base_healing_amt + (this.entries[i].healing_per_lvl * this.slevel["value"]);
        }
    };
    
    this.markClicked = function(entry) {
        if (entry.mark == "normal") {
            entry.mark = "keep";
        }
        else if (entry.mark == "keep") {
            entry.mark = "remove";
        }
        else if (entry.mark == "remove") {
            entry.mark = "normal";
        }
    };

	this.setColSort = function (colName) {
        if (this.sortCol == colName)
		    this.sortRev = !this.sortRev;
		this.sortCol = colName;
	}
}

angular.module('xeenTool').filter('spelllist', function() {
    return function(input, list) {
        var fspells = [];

        for (var i =0; i < input.length; ++i) {
            if (input[i].spell_lists.indexOf(list) > -1) {
                fspells.push(input[i]);
            }
        }

        return fspells;
    };
});

angular.module('xeenTool').filter('spelltype', function() {
    return function(input, type) {
        var fspells = [];

        for (var i =0; i < input.length; ++i) {
            if (type == "Damage" && input[i].dmg_type != "none") {
                fspells.push(input[i]);
            } else if (type == "Healing" && input[i].healing_per_lvl != 0) {
                fspells.push(input[i]);
            } else if (type == "Healing" && input[i].base_healing_amt != 0) {
                fspells.push(input[i]);
            }
        }

        return fspells;
    };
});
