angular.module('xeenTool').component('armortab', {
	controller: ArmorTabController,
    bindings: {
        armors: '<', 
        qualities: '<', 
        },
	templateUrl: 'app/components/armortab/armortab.template.html',
});

function ArmorTabController() {
	this.sortCol = "t_ac";
	this.sortRev = true;
    this.sarmor = null;
    this.saquality = null;

    this.aid = 0;

    this.aentries = []; 

    this.addClicked = function() {
        entry = {
            "aname": this.sarmor["name"],
            "ac": this.sarmor["ac"],
            "classes": this.sarmor["classes"],
            "type": this.sarmor["type"],
            "qname": this.saquality["name"],
            "enhancement": this.saquality["enhancement"],
            "emagnitude": this.saquality["emagnitude"],
            "acbonus": this.saquality["acbonus"],
            "mark": "normal",
            "t_ac": this.sarmor["ac"] + this.saquality["acbonus"],
            "id": this.aid,
        };
        this.aentries.push(entry);
        this.aid++;
    };

    this.clearClicked = function() {
        for (var i = 0; i < this.aentries.length; ++i) {
            if (this.aentries[i].mark != "keep") {
                this.aentries.splice(i, 1);
                i--;
            }
        }
    };

    this.removeClicked = function(id) {
        for (var i = 0; i < this.aentries.length; ++i) {
            if (this.aentries[i].id == id) {
                this.aentries.splice(i, 1);
                i--;
            }
        }
    };
    
    this.markClicked = function(wentry) {
        if (wentry.mark == "normal") {
            wentry.mark = "keep";
        }
        else if (wentry.mark == "keep") {
            wentry.mark = "remove";
        }
        else if (wentry.mark == "remove") {
            wentry.mark = "normal";
        }
    };
    
	this.setColSort = function (colName) {
        if (this.sortCol == colName)
		    this.sortRev = !this.sortRev;
		this.sortCol = colName;
	}
}

