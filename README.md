# Xeen Tool
## URL
http://t_stark.gitlab.io/xeen_tool/
## Purpose
This is a web tool to make playing Might and Magic World of Xeen slightly less tedious.
## Features
### Weapon Comparison Viewer
stub
### Armor Comparision Viewer
stub
### Spell Comparision Views
stub

# Notes
* I did not include the value calculations in this. Note that value does NOT always corrospond to quality in general.
## Weapons
* Some weapons have an addition quality which provides a damage multiplier against specific enemy types
## Accessories
* Qualities like Iron/Steel/Gold don't provide AC when on accessories (I'm not sure about "armored") they do increase value
## Spells
* Spells will do someting like "2-12" damage per level. I'm not sure if this means there is a dice roll involved or not. I'm assuming the distributions are weighted evenly.
* Apparently "all" and "group" mean different things. I'm not sure that I have this right on all spells.
